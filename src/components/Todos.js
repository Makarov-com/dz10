import * as React from "react";
import { useSelector } from "react-redux";


function UserInfo(props) {

    const {
        users,
        selectedUserId,
        isLoading,
        responseError
    } = useSelector((state) => state.usersManager);

    let array = users.filter(item => item.id === selectedUserId)

    return (

        responseError !== null && responseError !== true && isLoading !== true
            ? (
                <div>
                    {
                        array.map((item) => (
                            <div>
                                <p>Name: {item.name}</p>
                                <p>Username: {item.username}</p>
                                <p>Email: {item.email}</p>
                            </div>
                            
                        ))
                    }

                </div>


            )
            : null


    );
}

export default UserInfo;