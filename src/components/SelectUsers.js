import { useSelector, useDispatch } from "react-redux";
// import { useEffect } from "react"
import {
    FormControl,
    InputLabel,
    Select,
    MenuItem
} from "@material-ui/core";
import { setSelectedUser } from "../redux/actions";
import Alert from '@mui/material/Alert';
import { fetchUsers, setLoading } from '../redux/actions/usersManager'
import Spinner from "./Spinner" 
import Todos from "./Todos"


const SelectUsers = () => {
    const {
        users,
        selectedUserId,
        isLoading,
        responseError
    } = useSelector((state) => state.usersManager);
    const dispatch = useDispatch();

    const onChange = (e) => {
        const { value } = e.target;
        dispatch(setSelectedUser(value));
        dispatch(fetchUsers(dispatch(setLoading(true))))

    }


    return (
        <>
            <FormControl style={{ width: "100%", margin: "48px 0" }}>
                <InputLabel id="demo-simple-select-label">Select Users</InputLabel>
                <Select
                    value={selectedUserId}
                    onChange={onChange}
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                >
                    {!!users.length &&
                        users.map(({ id, name }) => <MenuItem key={id} value={id}>{name}</MenuItem>)
                    }
                </Select>
                <Todos />
                {responseError === true
                    ? (<Alert severity="error">Сталась помилка, перезавантажте сторінку!</Alert>)
                    : null

                }
                {isLoading === true
                    ? (<Spinner />)
                    : null
                }
            </FormControl>

        </>
    );
};

export default SelectUsers;
