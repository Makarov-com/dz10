import {
    CircularProgress,
    Box
} from "@material-ui/core";
import  FocusLock  from 'react-focus-lock';

const Spinner = (props) => {

    return (

        <FocusLock >
            < Box sx={{ display: "flex", position: "fixed", left: 0, top: 0, width: "100%", opacity: 0.9, height: "100%", bgcolor: 'white', justifyContent: "center", alignItems: "center" }} >

                <CircularProgress size={250} />

            </Box >
        </FocusLock> 


        // </div>
    );
};

export default Spinner;
