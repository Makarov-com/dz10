export const USERS_MANAGER_SET_USERS = "usersManager/SET_USERS";
export const USERS_MANAGER_SET_SELECTED_USER = "usersManager/SET_SELECTED_USER";
export const USER_MANAGER_SET_LOADING = "userManager/SET_LOADING";
export const USER_MANAGER_SET_ERROR = "userManager/SET_ERROR";

const REQUEST_TIME = 3000;

export const setUsers = (payload) => ({
    type: USERS_MANAGER_SET_USERS,
    payload
});

export const setSelectedUser = (payload) => ({
    type: USERS_MANAGER_SET_SELECTED_USER,
    payload
});

export const setLoading = (payload) => ({
    type: USER_MANAGER_SET_LOADING,
    payload
});

export const setError = (payload) => ({
    type: USER_MANAGER_SET_ERROR,
    payload
});
export const fetchUsers = () => (dispatch) => {
    fetchRequest("https://jsonplaceholder.typicode.com/users", dispatch)
}
export const fetchTodos = () => (dispatch) => {
    fetchRequest("https://jsonplaceholder.typicode.com/todos", dispatch)
}

/**
 * Request
 * @param {string} url
 * @return {Promise|Error}
 */
function fetchRequest(url, dispatch) {
    const rnd = Math.round(Math.random());
    console.log(rnd)
    // dispatch(setLoading(true));
    return new Promise((resolve, reject) => {


        setTimeout(() => {

            if (!rnd) {
                const error = new Error("An error occurred while executing the request.");
                dispatch(setError(true));
                dispatch(setLoading(false))
                return resolve(error);
            }

            dispatch(setLoading(false))
            dispatch(setError(false))


            fetch(url)
                .then((response) => resolve(response));
        }, REQUEST_TIME);
    });
}
