import { combineReducers } from "redux";
import usersManagerReducer from "./usersManager";

const rootReducer = combineReducers({
    usersManager: usersManagerReducer
});

export default rootReducer;
