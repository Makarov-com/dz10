import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { setUsers } from "./redux/actions";
import { Container } from "@material-ui/core";
import SelectUsers from "./components/SelectUsers";
import './App.css';
import { API_USERS_URI } from "./constants.json";

function App() {
  const dispatch = useDispatch();
  // const {
  //   isLoading,
  //   responseError
  // } = useSelector((state) => state.usersManager);
  useEffect(() => {
    fetch(API_USERS_URI)
      .then((response) => response.json())
      .then((data) => dispatch(setUsers(data)));
  }, [dispatch]);

  return (
    <Container>
      <SelectUsers />

    </Container>

  );
}

export default App;
